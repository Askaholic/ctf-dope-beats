<?php
session_start();

if(empty($_SESSION['username'])) {
    header('Location: login.php');
}
else if(isset($_GET['logout']) && $_GET['logout'] == 'true') {
    session_unset();
    session_destroy();
    header('Location: login.php');
}

define('SERVER','localhost');
define('USER', 'dope-beats.com');
define('PASS', 'DopeMelancholySQL42HYPERDRIVE*');
define('DB', 'dope_beats_db');
define('USER_TABLE', 'users');
define('FLAG_TABLE', 'flags');

$is_admin = $_SESSION['is_admin'];

$conn = new mysqli(SERVER, USER, PASS, DB);

$c_points;

$result = $conn->query("SELECT id, points FROM " . FLAG_TABLE);
if(!empty($result)) {
    while($row = $result->fetch_assoc()) {
        $c_points[(string) $row['id']] = intval($row['points']);
    }
}

?>

<html>
    <head>
        <title>Dope Scoreboard</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link rel="shortcut icon" href="/images/favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="/css/main.css" />
        <link rel="stylesheet" type="text/css" href="/css/style.css" />
        <link href='https://fonts.googleapis.com/css?family=Terminal+Dosis' rel='stylesheet' type='text/css' />
    </head>
    <body>
        <div class="header">
            <h1><a href="https://www.dope-beats.com">Dope Beats</a></h1>
            <?php
            echo '<h4>Logged in as ' . $_SESSION['displayname'] . '<a href="profile.php?logout=true">Logout</a></h4>';
            ?>
	</div>
        <div class="container">
            <div class="centered-wrapper">
                <div class="content">
                    <ul class="ca-menu">
                        <li>
                            <a href="profile.php?page=profile">
                                <span class="ca-icon">S</span>
                                <div class="ca-content">
                                        <h2 class="ca-main">My Profile</h2>
                                </div>
                            </a>
                        </li>
                        <?php
                            if($is_admin) {
                                echo '<li>
                                    <a href="profile.php?page=users">
                                        <span class="ca-icon">U</span>
                                        <div class="ca-content">
                                                <h2 class="ca-main">Users</h2>
                                        </div>
                                    </a>
                                </li>';
                            }
                        ?>
                        <li>
                            <a href="profile.php?page=challenge">
                                <span class="ca-icon">q</span>
                                <div class="ca-content">
                                        <h2 class="ca-main">Challenges</h2>
                                </div>
                            </a>
                        </li>
                        <li style="border-color: #30b7ff;">
                            <a href="scoreboard.php">
                                <span class="ca-icon" style="color: #30b7ff;">_</span>
                                <div class="ca-content">
                                        <h2 class="ca-main" style="color: #30b7ff;">Scoreboard</h2>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <?php
            echo '<div class="container">
                    <div class="centered-wrapper">
                        <h1>Scores</h1>
                    <div class="content" style="padding: 10px 10px 10px 10px;margin: 5px auto;max-width:50vw;text-align:left;" id="challenge-wrapper">';
            $result = $conn->query("SELECT displayname, challenges FROM " . USER_TABLE);
            $u_points;
            while($row = $result->fetch_assoc()) {
                $challenges = explode(" ", $row['challenges']);
                $challenges_length = count($challenges);
                $total_points = 0;
                for($c = 0; $c < $challenges_length; $c++) {
                    $total_points += $c_points[$challenges[$c]];
                }
                $u_points[$row['displayname']] = $total_points;
            }
            arsort($u_points);
            foreach ($u_points as $displayname => $points) {
                echo '<div class="sub-content">' . $displayname . ' - ' . $points . '
                      </div>';
            }
            echo '</div>
                </div>
            </div>'
        ?>
        <div class="footer">
            
        </div>
        
    </body>
</html>

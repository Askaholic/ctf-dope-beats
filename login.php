<?php
session_start();

define('SERVER','localhost');
define('USER', 'dope-beats.com');
define('PASS', 'DopeMelancholySQL42HYPERDRIVE*');
define('DB', 'dope_beats_db');
define('USER_TABLE', 'users');
define('MAX_USERS', 100);
define('REDIRECT_PAGE', 'profile.php');

$form_username = $error = "";

$conn = new mysqli(SERVER, USER, PASS, DB);
if($conn->connect_error) {
    die('Could not reach database: ' . $conn->connect_error);
}
else {
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        $form_username = fix_data($_POST['username']);
        $real_username = strtolower($form_username);
        $form_password = $_POST['password'];
        $form_confirmp = $_POST['confpass'];
        if(empty($form_username)) {
            $error = '* You must enter a username';
        }
        else if(strlen($form_username) > 30) {
            $error = '* Username may not exceed 30 characters';
            $form_username = "";
        }
        else if(!preg_match("/^[a-zA-Z0-9 ]*$/",$form_username)) {
            $error = '* Username contains illegal characters';
            $form_username = "";
        }
        if(empty($form_password)) {
            $error = '* You must enter a password';
        }
        else if(!empty($form_username)) {
            if(!$_POST['signup']) {
                //If they wish to login, and username is valid with no empty password
                $prep = $conn->prepare("SELECT password FROM " . USER_TABLE . " WHERE username = ? LIMIT 1");
                $prep->bind_param("s", $real_username);
                $result = "";
                $prep->bind_result($result);
                $prep->execute();
                $prep->fetch();
                $prep->close();
                if(!empty($result)) {
                    //A result was found in the database
                    if(password_verify($form_password, $result)) {
                        //The password matches the hash. Create session, redirect to profile page
//                                echo "Login success";
                        setSessionAndRedirect($conn, $real_username, $form_username);
                    }
                    else {
                        $error = "* Invalid username or password";
                        $form_username = "";
                    }
                }
                else {
                    //The user was not found in the database
                    $error = "* Invalid username or password";
                    $form_username = "";
                }
            }
            else if($form_password != $form_confirmp) {
                $error = "* Passwords do not match";
            }
            else {
                //The user wishes to sign up
                if($conn->query("SELECT 1 FROM " . USER_TABLE . " LIMIT 1") == false) {
                    //Create the user tables if non exist yet
                    $sql = "CREATE TABLE " . USER_TABLE . " (
                        id SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                        username VARCHAR(30) NOT NULL,
                        displayname VARCHAR(30) NOT NULL,
                        password VARCHAR(255) NOT NULL,
                        admin_priv CHAR(1) NOT NULL,
                        register_date DATETIME DEFAULT GETDATE());";
                    if($conn->query($sql) != true) {
                        $error = "* Could not create table";
                    }
                    else {
                        if(add_user($conn, USER_TABLE, $form_username, $form_password)) {
                            setSessionAndRedirect($conn, $real_username, $form_username);
                        }
                    }
                }
                else {
                    //We need to check if someone else already claimed that username
                    $prep = $conn->prepare("SELECT username FROM " . USER_TABLE . " WHERE username = ? LIMIT 1");
                    $prep->bind_param("s", $real_username);
                    $prep->bind_result($result);
                    $prep->execute();
                    $prep->fetch();
                    $prep->close();

                    if(empty($result) && $conn->insert_id < MAX_USERS) {
                        if(add_user($conn, USER_TABLE, $form_username, $form_password)) {
//                                    echo "Created user</br>";
                            setSessionAndRedirect($conn, $real_username, $form_username);
                        }
                        echo "</br>You are user number $conn->insert_id";
                    }
                    else {
                        $error = "* That username is already taken";
                    }
                }
            }
        }
    }
}

function add_user($conn, $table, $user, $pass) {
    $hash = password_hash($pass, PASSWORD_DEFAULT);
    $prep = $conn->prepare("INSERT INTO $table (username, displayname, password, admin_priv, register_date) VALUES (?,?,?,?,?)");
    $prep->bind_param('sssss', $user_real, $user, $hash, $admin, $date);

    $user_real = strtolower($user);
    $admin = "N";
    $date = date("Y-m-d H:i:s");
    $ret_val = $prep->execute();
    $prep->close();
    return $ret_val;
}

function setSessionAndRedirect($conn, $uname, $dname) {
    $stmt = $conn->prepare("SELECT admin_priv, register_date FROM " . USER_TABLE . " WHERE username = '$uname'");
    $stmt->bind_result($r_admin, $r_date);
    $stmt->execute();
    $stmt->fetch();
    $_SESSION['username'] = $uname;
    $_SESSION['displayname'] = $dname;
    $_SESSION['is_admin'] = $r_admin == "Y" ? true : false;
    $_SESSION['reg_date'] = $r_date;
    header('Location: ' . REDIRECT_PAGE);
}

function fix_data($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
?>
<html>
    <head>
        <title>Login to DopeCTF</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link rel="shortcut icon" href="/images/favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="/css/main.css" />
        <link rel="stylesheet" type="text/css" href="/css/style.css" />
        <link href='https://fonts.googleapis.com/css?family=Terminal+Dosis' rel='stylesheet' type='text/css' />
    </head>
    <body>
        <div class="header">
            <h1><a href="/index.php">Dope Beats</a></h1>
	</div>
        <script>
        $(document).ready(function() {
            $("#signup").click(function() {
                if($("#signup").is(':checked')) {
                    $("#confp").show();
                    $("#conf").show();
                    $("#button").attr('value', 'Sign Up');
                }
                else {
                    $("#confp").hide();
                    $("#conf").hide();
                    $("#button").attr('value', 'Login');
                }
            });
            $("#showpass").click(function() {
               if($("#showpass").is(':checked')) {
                   $("#pass").attr('type', 'text');
                   $("#conf").attr('type', 'text');
               } 
               else {
                   $("#pass").attr('type', 'password');
                   $("#conf").attr('type', 'password');
               }
            });
        });   
        </script>
        <div class="centered-wrapper">
            <div class="content">
                <form method="post" style="padding: 0 2vw 0 2vw;" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                    <span><?php echo $error . "<br/>"?></span>
                    <label>Username</label><br/>
                    <input name="username" type="text" id="un" maxlength="30" value="<?php echo $form_username;?>"><br/>
                    <label>Password</label><br/>
                    <input name="password" type="password" id="pass"><br/>
                    <label hidden id="confp">Confirm Password</label><br/>
                    <input name="confpass" type="password" id="conf" hidden><br/>
                    <div style="text-align:left">
                        <label><input name="showpass" type="checkbox" id="showpass" style="margin-right: 10px"/>Show password</label><br/>
                        <label><input name="signup" type="checkbox" id="signup" style="margin-right: 10px"/>Create account</label><br/>
                    </div>
                    <input name="submit" type="submit" value="Login" id="button">
                </form>
            </div>
        </div>
    </body>
</html>
